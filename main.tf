provider "google" {
  project = "kelompok-4-349509"
  region  = "asia-southeast2"
  zone    = "asia-southeast2-a"
}

# Create a single Compute Engine instance Master
resource "google_compute_instance" "k4-master" {
  name         = "p-k4-master-01"
  machine_type = "e2-medium"
  zone         = "asia-southeast2-a"
  tags         = ["http-server", "https-server", "k4-kubernetes"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    network = "default"

    access_config {
      # Include this section to give the VM an external IP address
    }
  }
}

# Create more Compute Engine instance Workers
resource "google_compute_instance" "k4-worker" {
  for_each     = toset(["01", "02", "03", "04", "05"])
  name         = "p-k4-worker-${each.key}"
  machine_type = "e2-micro"
  zone         = "asia-southeast2-a"
  tags         = ["http-server", "https-server", "k4-kubernetes"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    network = "default"

    access_config {
      # Include this section to give the VM an external IP address
    }
  }
}